# Vacuum Capacitance Gauge MKS

**DEPRECATED**
**Please use vac_ctrl_mks946_937b with the built-in vac_gauge_mks_vgd snippet**

EPICS module to read/write data for MKS vacuum capacitance gauge.

IOC does not directly communicate with the gauge, the gauge is actully connected to a vacuum controller. IOC communicated to the controller and controller provides the gauge data. Controllers can have multiple gauges in deifferent channels.


The modules arrangement should be something like:
```
IOC-|
    |-vac-ctrl-mks946_937b
    |      |-vac-gauge-mks-vgp (Pirani gauge)
    |      |-vac-gauge-mks-vgp (Pirani gauge)
    |      |-vac-gauge-mks-vgc (Cold cathode gauge)
    |
    |-vac-ctrl-mks946_937b
    |      |-vac-gauge-mks-vgp (Pirani gauge)
    |      |-vac-mfc-mks-gv50a (Mass flow controller)
```

This allows EPICS modules for gauges to be re-used and any combination of controller and gauges to be built from existing modules.

**This version of the module requires at least v3.0.4 of vac_ctrl_mks946_937b**

## Startup Examples

`iocsh -r vac_ctrl_mks946_937b,catania -c 'requireSnippet(vac_ctrl_mks946_937b_ethernet.cmd,"DEVICENAME=LNS-LEBT-010:VAC-VEVMC-01100, IPADDR=10.4.0.213, PORT=4004")' -r vac_gauge_mks_vgd,catania -c 'requireSnippet(vac_gauge_mks_vgd.cmd, "DEVICENAME=LNS-LEBT-010:VAC-VGD-10000, CONTROLLERNAME=LNS-LEBT-010:VAC-VEVMC-01100, CHANNEL=3, CHANNEL_C=1, RELAY1=5, RELAY2=6")'`

If ran as proper ioc service:
```
epicsEnvSet(DEVICENAME, "LNS-LEBT-010:VAC-VGD-10000")
epicsEnvSet(CONTROLLERNAME, "LNS-LEBT-010:VAC-VEVMC-01100")
epicsEnvSet(CHANNEL, "3")
epicsEnvSet(CHANNEL_C, "3")
epicsEnvSet(RELAY1, "5")
epicsEnvSet(RELAY2, "6")
require vac_gauge_mks_vgd, 2.0.1-catania
< ${REQUIRE_vac_gauge_mks_vgd_PATH}/startup/vac_gauge_mks_vgd.cmd
```
